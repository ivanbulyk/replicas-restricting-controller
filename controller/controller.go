package controller

import (
	"context"

	samplev1alpha1 "gitlab.com/ivanbulyk/replicas-restricting-controller/api/types/v1alpha1"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

// newDeployment creates a new Deployment for a MyDemo resource. It also sets
// the appropriate OwnerReferences on the resource so handleObject can discover
// the MyDemo resource that 'owns' it. MyDemo
func NewDeployment(mydemo *samplev1alpha1.MyDemo) *appsv1.Deployment {
	labels := map[string]string{
		"app":        "nginx",
		"controller": mydemo.Name,
	}
	return &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      mydemo.Spec.DeploymentName,
			Namespace: mydemo.Namespace,
			OwnerReferences: []metav1.OwnerReference{
				*metav1.NewControllerRef(mydemo, samplev1alpha1.SchemeGroupVersion.WithKind("MyDemo")),
			},
		},
		Spec: appsv1.DeploymentSpec{
			Replicas: mydemo.Spec.Replicas,
			Selector: &metav1.LabelSelector{
				MatchLabels: labels,
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: labels,
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{
						{
							Name:  "nginx",
							Image: "nginx:latest",
						},
					},
				},
			},
		},
	}
}

func GetDeployments(clientset *kubernetes.Clientset, ctx context.Context, namespace string) ([]appsv1.Deployment, error) {

	list, err := clientset.AppsV1().Deployments(namespace).List(ctx, metav1.ListOptions{})
	if err != nil {
		return nil, err
	}
	return list.Items, nil
}

func GetReplicaSets(clientset *kubernetes.Clientset, ctx context.Context, namespace string) ([]appsv1.ReplicaSet, error) {

	list, err := clientset.AppsV1().ReplicaSets(namespace).List(ctx, metav1.ListOptions{})
	if err != nil {
		return nil, err
	}
	return list.Items, nil
}

func GetSpecificDeployment(clientset *kubernetes.Clientset, ctx context.Context, namespace string, name string) (*appsv1.Deployment, error) {

	list, err := clientset.AppsV1().Deployments(namespace).Get(ctx, name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}
	return list, nil
}

func GetPods(clientset *kubernetes.Clientset, ctx context.Context, namespace string, name string) (*corev1.PodList, error) {

	list, err := clientset.CoreV1().Pods(namespace).List(ctx, metav1.ListOptions{})
	if err != nil {
		return nil, err
	}
	return list, nil
}
