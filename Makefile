.PHONY: build start 

build:
	go build -o ctrl .

start:

	go run main.go -kubeconfig ~/.kube/config
	
.DEFAULT_GOAL := start