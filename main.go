package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/ivanbulyk/replicas-restricting-controller/api/types/v1alpha1"
	clientV1alpha1 "gitlab.com/ivanbulyk/replicas-restricting-controller/clientset/v1alpha1"
	"gitlab.com/ivanbulyk/replicas-restricting-controller/controller"

	appsv1 "k8s.io/api/apps/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

var kubeconfig string

func init() {
	flag.StringVar(&kubeconfig, "kubeconfig", "", "path to Kubernetes config file")
	flag.Parse()
}

func main() {
	// Set logging output to standard console out
	log.SetOutput(os.Stdout)

	stop := make(chan os.Signal, 1)
	signal.Notify(stop,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)

	var config *rest.Config
	var err error
	ctx := context.Background()

	namespace := "default"

	if kubeconfig == "" {
		log.Printf("using in-cluster configuration")
		config, err = rest.InClusterConfig()
	} else {
		log.Printf("using configuration from '%s'", kubeconfig)
		config, err = clientcmd.BuildConfigFromFlags("", kubeconfig)
	}

	if err != nil {
		panic(err)
	}

	v1alpha1.AddToScheme(scheme.Scheme)

	clientSet, err := clientV1alpha1.NewForConfig(config)
	if err != nil {
		panic(err)
	}

	clientset_kubernetes := kubernetes.NewForConfigOrDie(config)

	mydemos, err := clientSet.MyDemos("default").List(ctx, metav1.ListOptions{})
	if err != nil {
		panic(err)
	}

	deploymentName := mydemos.Items[0].Spec.DeploymentName

	deployment, err := controller.GetSpecificDeployment(clientset_kubernetes, ctx, namespace, deploymentName)
	if err != nil {
		// If the resource doesn't exist, we'll create it
		if errors.IsNotFound(err) {
			deployment, err = clientset_kubernetes.AppsV1().Deployments(namespace).Create(ctx, controller.NewDeployment(&mydemos.Items[0]), metav1.CreateOptions{})
		}
		fmt.Println(err)
	}

	go func() {
		for {
			CoreLogic(clientset_kubernetes, mydemos, namespace, deployment, deploymentName, ctx)
			time.Sleep(1 * time.Minute)
			continue
		}

	}()
	log.Println("Started watching replicas number ..")

	<-stop
	log.Println("caught stop signal")

	log.Println("Shutting down...")

}

func CoreLogic(clientset_kubernetes *kubernetes.Clientset, mydemos *v1alpha1.MyDemoList, namespace string, deployment *appsv1.Deployment, deploymentName string, ctx context.Context) {
	if mydemos.Items[0].Spec.Replicas != nil && *mydemos.Items[0].Spec.Replicas != *deployment.Spec.Replicas {

		err := clientset_kubernetes.AppsV1().Deployments(namespace).Delete(ctx, deploymentName, metav1.DeleteOptions{})
		if err != nil {
			fmt.Println(err)
		}
		deployment, err = clientset_kubernetes.AppsV1().Deployments(namespace).Create(ctx, controller.NewDeployment(&mydemos.Items[0]), metav1.CreateOptions{})
		if err != nil {
			fmt.Println(err)
		}

	}
}
