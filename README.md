# A client library for your Custom Resource and a simple Kubernetes operator that reacts on changes made to Custom Resources, built on top of it.

## To run the app/operator: 

- # create a CustomResourceDefinition
kubectl create -f kubernetes/mydemo-crd.yaml

- # create a custom resource of type MyDemo
kubectl create -f kubernetes/example-mydemo.yaml

- `make` in project root 
                             or
- `make build`in project root
- `./ctrl -kubeconfig ~/.kube/config`


# check replicasets number created or scaled to be in number other than 2
# in every 1 minute it should be arranged to be equal to 2
kubectl get replicasets