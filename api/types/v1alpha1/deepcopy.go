package v1alpha1

import (
	runtime "k8s.io/apimachinery/pkg/runtime"
)

// DeepCopyInto is a deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *MyDemo) DeepCopyInto(out *MyDemo) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ObjectMeta.DeepCopyInto(&out.ObjectMeta)
	out.Spec = in.Spec
}

// DeepCopy is a deepcopy function, copying the receiver, creating a new MyDemo.
func (in *MyDemo) DeepCopy() *MyDemo {
	if in == nil {
		return nil
	}
	out := new(MyDemo)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is a deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *MyDemo) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is a deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *MyDemoList) DeepCopyInto(out *MyDemoList) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ListMeta.DeepCopyInto(&out.ListMeta)
	if in.Items != nil {
		in, out := &in.Items, &out.Items
		*out = make([]MyDemo, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is a deepcopy function, copying the receiver, creating a new MyDemoList.
func (in *MyDemoList) DeepCopy() *MyDemoList {
	if in == nil {
		return nil
	}
	out := new(MyDemoList)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is a deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *MyDemoList) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is a deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *MyDemoSpec) DeepCopyInto(out *MyDemoSpec) {
	*out = *in
	if in.Replicas != nil {
		in, out := &in.Replicas, &out.Replicas
		*out = new(int32)
		**out = **in
	}
	return
}

// DeepCopy is a deepcopy function, copying the receiver, creating a new MyDemoSpec.
func (in *MyDemoSpec) DeepCopy() *MyDemoSpec {
	if in == nil {
		return nil
	}
	out := new(MyDemoSpec)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is a deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *MyDemoStatus) DeepCopyInto(out *MyDemoStatus) {
	*out = *in
	return
}

// DeepCopy is a deepcopy function, copying the receiver, creating a new MyDemotatus.
func (in *MyDemoStatus) DeepCopy() *MyDemoStatus {
	if in == nil {
		return nil
	}
	out := new(MyDemoStatus)
	in.DeepCopyInto(out)
	return out
}
